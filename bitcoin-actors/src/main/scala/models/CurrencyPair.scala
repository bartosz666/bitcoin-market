package models

import shapeless._
import shapeless.syntax.std.traversable.traversableOps

case class CurrencyPair(primary: String, secondary: String) {

  def reverse = CurrencyPair(secondary, primary)

}

object CurrencyPairConversions {

  implicit def underScoreSeparatedString(pair: String): CurrencyPair =
    CurrencyPair.tupled(pair.split('_').toHList[String::String::HNil].get.tupled)


  implicit def currencyPairFromTuple(source: (String, String)) = {
    val (primary, secondary) = source
    CurrencyPair(primary, secondary)
  }

  implicit class CurrencyPairsOperations(val pairSets: Iterable[Seq[CurrencyPair]]) extends AnyVal {

    implicit def filterSinglePairs: Set[CurrencyPair] = {
      val interestingPairs = pairSets.flatten.toList.groupBy(pair => pair).filter {
        case (pair, occurrences) => occurrences.size > 1
      }
      interestingPairs.map(_._1).toSet
    }

  }

}