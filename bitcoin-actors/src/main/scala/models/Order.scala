package models

import org.joda.time.DateTime
import actors.ExchangeAPI.Exchange

/**
 * Order information.
 */
case class Order( id: String,
                  exchange: Exchange,
                  pair: CurrencyPair,
                  created: DateTime,
                  orderType: OrderType,
                  price: BigDecimal,
                  quantity: BigDecimal,
                  originalQuantity: BigDecimal)