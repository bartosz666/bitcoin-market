package models

import org.joda.time.DateTime

/**
 * Single arbitrage oportunity
 */
case class ArbitrageOperation(sellOrder: OrderRequest, buyOrder: OrderRequest, created: DateTime = new DateTime())

object ArbitrageOperation {
//  def createOrder(spread: MarketSpread): ArbitrageOperation = {
//    val pair = spread.market1.currencyPair
//    val quantity = short.bid.get.volume.min(long.ask.get.volume)
//    val sellOrder = Order(Sell, short.exchange, pair, quantity, short.bid.get.price)
//    val buyOrder  = Order(Buy, long.exchange, pair, quantity, long.ask.get.price)
//    ArbitrageOperation(sellOrder, buyOrder)
//  }

}

case class ClosedArbitrageOperation(op: ArbitrageOperation, spreadClosedAt: DateTime)