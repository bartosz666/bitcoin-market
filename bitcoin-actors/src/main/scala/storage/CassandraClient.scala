package storage

import scala.concurrent.{Await, blocking}
import com.datastax.driver.core.Cluster
import com.typesafe.config.Config
import com.newzly.phantom.CassandraTable
import scala.concurrent.duration._
import com.typesafe.scalalogging.slf4j.LazyLogging

class CassandraClient(config: Config) extends LazyLogging {

  private[storage] val cassandraConfig = config.getConfig("storage.cassandra")

  private[storage] val keySpace = cassandraConfig.getString("keyspace")

  lazy val cluster = Cluster.builder()
    .addContactPoint(cassandraConfig.getString("endpoint"))
    .withPort(cassandraConfig.getInt("port")) //default transport port
    .withoutJMXReporting()
    .withoutMetrics()
    .build()


  lazy implicit val session = blocking {
    cluster.connect(keySpace)
  }

  def createKeySpace() = {
    val session = cluster.connect()
    if(cluster.getMetadata.getKeyspace(keySpace) == null) {
      session.execute(s"CREATE KEYSPACE $keySpace WITH replication = {'class':'SimpleStrategy', 'replication_factor':1};")
    }
  }

  def createSchema(tables: CassandraTable[_,_]*) {

    createKeySpace()
    tables.foreach { table =>
      if(cluster.getMetadata.getKeyspace(keySpace).getTable(table.tableName) == null) {
        logger.info(s"Creating ${table.tableName} table")
        Await.result(table.create.future(), Duration(5, SECONDS))
      }
    }
  }
}

