package storage

import com.newzly.phantom.CassandraTable
import com.datastax.driver.core.Row
import com.newzly.phantom.Implicits._
import models.User
import scala.concurrent.Future

/**
 * Authenticated users access
 */
sealed class UserRecord(client: CassandraClient)
  extends CassandraTable[UserRecord, User] {

  override val tableName = "user"

  implicit lazy val session = client.session

  object username extends StringColumn(this) with PartitionKey[String]

  object seed extends StringColumn(this)

  object password extends StringColumn(this)

  override def fromRow(row: Row): User = {
    User(username(row), password(row), seed(row))
  }

  def userByName(username: String): Future[Option[User]] = {
    select.allowFiltering().where(_.username eqs username).one()
  }
}
