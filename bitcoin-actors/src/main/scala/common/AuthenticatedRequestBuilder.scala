package common

import scala.compat.Platform
import javax.crypto.spec.SecretKeySpec
import javax.crypto.Mac
import spray.http._
import spray.http.HttpRequest
import spray.http.HttpHeaders.RawHeader
import spray.client.pipelining._

/**
 * Private request builder.
 */
class AuthenticatedRequestBuilder(
                             url: String,
                             publicKey: String,
                             privateKey: String) {

  val keyspec: SecretKeySpec =
    new SecretKeySpec(privateKey.getBytes("UTF-8"), "HmacSHA512")

  val mac = Mac.getInstance("HmacSHA512")

  mac.init(keyspec)

  def nonce = Platform.currentTime / 1000

  /**
   * Create data signature string
   */
  def buildRequest(data: (String, String)*): HttpRequest = {
    val dataAndNonce = ("nonce" -> nonce.toString) +: data
    val queryString = dataAndNonce.map(kv => s"${kv._1}=${kv._2}").mkString("&")

    val sign = mac.doFinal(queryString.getBytes("UTF-8")).map(b => "%02X" format (b & 0xff)).mkString
    val headers: List[HttpHeader] = List(
      RawHeader("SIGN", sign.toLowerCase),
      RawHeader("KEY", publicKey)
    )
    Post(url, FormData(dataAndNonce)).withHeaders(headers)
  }

}
