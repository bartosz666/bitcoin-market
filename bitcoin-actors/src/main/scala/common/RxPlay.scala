package misc

import play.api.libs.iteratee.{Concurrent, Iteratee, Enumerator}
import rx.lang.scala.{Subscription, Observer, Observable}
import scala.util.{Failure, Success}
import play.api.libs.iteratee.Concurrent.Channel
import scala.concurrent.ExecutionContext.Implicits.global


object RxPlay {

  implicit def enumerator2Observable[T](enum: Enumerator[T]): Observable[T] = {
    Observable({ observer: Observer[T] =>
      enum (
        Iteratee.foreach(observer.onNext)
      ).onComplete {
        case Success(_) => observer.onCompleted()
        case Failure(e) => observer.onError(e)
      }

      new Subscription { override def unsubscribe() = {} }
    })
  }

  implicit def observable2Enumerator[T](obs: Observable[T]): Enumerator[T] = {
    Concurrent.unicast[T](onStart = { chan =>
      obs.subscribe(new ChannelObserver(chan))
    })
  }

  class ChannelObserver[T](chan: Channel[T]) extends Observer[T] {
    override def onNext(arg: T): Unit = chan.push(arg)
    override def onCompleted(): Unit = chan.end()
    override def onError(e: Throwable): Unit = chan.end(e)
  }
}