package common

import javax.crypto.spec.SecretKeySpec
import javax.crypto.Mac

object Crypto {

  def hmacSHA512(secret: String, data: String) = {
    val secretSpec = new SecretKeySpec(secret.getBytes, "HmacSHA1")
    val mac = Mac.getInstance("HmacSHA1")
    mac.init(secretSpec)
    val result: Array[Byte] = mac.doFinal(data.getBytes)
    result.map(_.toString).mkString
  }
}