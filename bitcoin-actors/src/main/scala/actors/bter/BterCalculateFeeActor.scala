package actors.bter

import akka.actor._
import actors.ExchangeAPI.TransactionFeeResponse
import models.CurrencyPair
import actors.ExchangeAPI.TransactionFeeRequest
import com.typesafe.config.Config

object BterCalculateFeeActor {

  def props(config: Config) = Props(classOf[BterCalculateFeeActor], config)

}

/**
 * Exchange meta-data actor. Returns fee for BTER transaction. BTER doesn't expose API for fee calculation
 * therefor the values returned are static based on configuration.
 */
class BterCalculateFeeActor(config: Config) extends Actor with ActorLogging {

  implicit val system = context.system

  var fees = Map[CurrencyPair, BigDecimal]()

  val defaultFee = config.getDouble("bter.fees.default")

  override def receive: Actor.Receive = {
    case req @ TransactionFeeRequest(pair, exchange) =>
      log.info(s"Sending fee value for $pair.")
      sender ! TransactionFeeResponse(pair, exchange, fees.getOrElse(pair, defaultFee))
  }
}
