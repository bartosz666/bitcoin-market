package actors.bter

import akka.actor.{ActorLogging, Actor, Props}
import akka.io.IO
import spray.can.Http
import spray.http._
import HttpMethods._
import spray.httpx.unmarshalling._
import models.{CurrencyPair, Ticker}
import spray.json._
import org.joda.time.DateTime
import spray.http.HttpRequest

object BterMarketDataActor {

  def props(currencyPair: CurrencyPair) = Props(new BterMarketDataActor(currencyPair))

}

/**
 * Client to BTRE exchange. It is created by an actor interested in receiving pricing updates in
 * recursive manner.
 */
class BterMarketDataActor(currencyPair: CurrencyPair)
  extends Actor
  with ActorLogging {

  val url = s"http://data.bter.com/api/1/ticker/${currencyPair.primary}_${currencyPair.secondary}"

  import context._

  class TickerUnmarshaller(currencyPair: CurrencyPair) extends Unmarshaller[Ticker] with JsonReader[Ticker] {

    /**
     * Expected response example:
     * {{{
     *   {
     *    result: "true",
     *    last: "0.00002770",
     *    high: "0.00003021",
     *    low: "0.00002676",
     *    avg: "0.00002873",
     *    sell: "0.00002899",
     *    buy: "0.00002689",
     *    vol_bqc: 38853.21,
     *    vol_btc: 1.116066
     *   }
     * }}}
     *
     * @param json JSON object reference
     * @return Returns Ticker object
     */
    override def read(json: JsValue): Ticker = {
      val ticker = json.asJsObject
      val last = BigDecimal(ticker.fields("last").asInstanceOf[JsString].value)
      val buy = BigDecimal(ticker.fields("buy").asInstanceOf[JsString].value)
      val sell = BigDecimal(ticker.fields("sell").asInstanceOf[JsString].value)
      val volume = ticker.fields(s"vol_${currencyPair.primary.toLowerCase}").asInstanceOf[JsNumber].value
      Ticker(currencyPair, "BTER", last, buy, sell, volume, new DateTime())
    }

    override def apply(entity: HttpEntity): Deserialized[Ticker] = {
      val parser = JsonParser(entity.asString)
      try {
        Right(parser.convertTo[Ticker](this))
      } catch {
        case ex: Throwable => Left(MalformedContent("Could not unmarshal price value", ex))
      }
    }
  }

  implicit val unmarshaller = new TickerUnmarshaller(currencyPair)


  /**
   * Invoked in given interval by scheduler. The action should request data update from corresponding API.
   */
  def sendUpdateRequest(): Unit = {
    IO(Http) ! HttpRequest(GET, Uri(url))
  }

  override def receive: Actor.Receive = ???
}
