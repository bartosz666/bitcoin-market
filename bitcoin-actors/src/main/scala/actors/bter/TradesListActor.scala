package actors.bter

import akka.actor.{Actor, ActorLogging}
import actors.ExchangeAPI.OrdersListRequest

class TradesListActor extends Actor with ActorLogging {

  override def receive: Receive = {
    case OrdersListRequest =>
      sender ! "Not implemented"
  }

}
