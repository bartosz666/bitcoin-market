package actors

import models.{Order, CurrencyPair}
import scala.concurrent.duration.FiniteDuration
import scala.collection.immutable.Seq

/**
 * Messages
 */
object ExchangeAPI {

  sealed trait ExchangeRequest

  type Exchange = String

  /**
   * Request for all currency pairs available.
   */
  case object CurrencyPairsRequest extends ExchangeRequest

  case class MarketInfoRequest(currencyPair: CurrencyPair, exchange: Exchange)
    extends ExchangeRequest

  case class TransactionFeeRequest(currencyPair: CurrencyPair, exchange: Exchange)
    extends ExchangeRequest

  case class StartRepeating(request: ExchangeRequest, interval: FiniteDuration)

  case class CurrencyPairsResponse(pairs: Seq[CurrencyPair])

  case class ExchangeOrder(price: BigDecimal, volume: BigDecimal)

  case object OrdersListRequest

  case class OrderListResponse(exchange: Exchange, orders: List[Order])


  /**
   * Top 1 offer of selling and buying
   */
  case class MarketInfo(currencyPair: CurrencyPair,
                        exchange: Exchange,
                        ask: Option[ExchangeOrder],
                        bid: Option[ExchangeOrder])

  /**
   *
   * @param currencyPair Currency pair this response refers to
   * @param exchange Exchange rate this response refers to
   * @param fee Value of fee as %
   */
  case class TransactionFeeResponse(currencyPair: CurrencyPair, exchange: Exchange, fee: BigDecimal)

  case class ErrorResponse(request: ExchangeRequest, message: String)


}
