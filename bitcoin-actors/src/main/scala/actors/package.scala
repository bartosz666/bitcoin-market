import actors.ExchangeAPI.ExchangeRequest
import akka.actor.ActorRef

/**
 * Created by fox on 10/03/14.
 */
package object actors {

  /**
   * Scheduled message
   * @param msg Message to execute
   * @param recipient Recipient of the result
   */
  case class PlannedMessage( msg: ExchangeRequest, recipient: ActorRef)

  /**
   * Generic acknowledgment
   */
  case object Ack

}
