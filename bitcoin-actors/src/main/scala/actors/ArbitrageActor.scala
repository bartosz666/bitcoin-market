package actors

import akka.actor.{Props, ActorRef, ActorLogging, Actor}
import akka.pattern.{AskTimeoutException, ask}
import models._
import scala.concurrent.duration._
import actors.ExchangeAPI._
import akka.util.Timeout
import actors.ExchangeAPI.ErrorResponse
import actors.ExchangeAPI.MarketInfo
import models.CurrencyPair
import models.CurrencyPairConversions._
import actors.ExchangeAPI.CurrencyPairsResponse
import scala.concurrent.Future

object ArbitrageActor {

  def props(exchangeApiActors: Map[Exchange, ActorRef]) = Props(new ArbitrageActor(exchangeApiActors))

}

/**
 * Handles all currency updates making arbitrage decisions
 */
class ArbitrageActor(exchangeApiActors: Map[Exchange, ActorRef]) extends Actor with ActorLogging {

  import context._

  private var rates: Map[Exchange, Map[CurrencyPair, MarketInfo]] = Map()

  private var deduplicators: Map[(CurrencyPair, (Exchange, Exchange)), ActorRef] = Map()

  case class ArbitrageDirection(long: MarketInfo, short: MarketInfo)

  override def preStart() = {
    initiateUpdates()
  }

  def initiateUpdates(): Unit = {
    implicit val timeout = Timeout( 30 seconds)
    val allPairs = Future.sequence(collectCurrencies)
    allPairs.foreach { data =>
      val commonPairs = data.map(_._2).filterSinglePairs
      for (
        pair <- commonPairs;
        ((exchange, actor), currencies) <- data if currencies.contains(pair)
      ) {
        log.info(s"Requesting updates from $actor every 10 seconds for $pair")
        actor ! StartRepeating(MarketInfoRequest(pair, exchange), 10 seconds)
      }
    }
  }

  private def collectCurrencies = {
    implicit val timeout = Timeout( 30 seconds)
    exchangeApiActors.map { exchangeActor =>
      val (_, apiActor) = exchangeActor
      val currenciesFuture = (apiActor ? CurrencyPairsRequest) flatMap {
        case er: ErrorResponse =>
          log.error(s"Unable to fetch currency list ${er.message}")
          Future.failed(new Exception(er.message))
        case CurrencyPairsResponse(currencies) =>
          Future.successful(currencies)
        case _ =>
          Future.failed(new Exception("Invalid response"))
      }
      currenciesFuture.map( exchangeActor -> _)
    }
  }

  override def receive: Receive = {
    case info: MarketInfo =>
      log.debug(s"New update: $info")
      if(!rates.isDefinedAt(info.exchange)
        || !rates(info.exchange).isDefinedAt(info.currencyPair)
        || rates(info.exchange)(info.currencyPair) != info) {
        processMarketChange(info)
      } else {
        log.debug(s"Ignoring redundant update $info")
      }
    case ErrorResponse(req, msg) =>
      log.error(s"Received error message for $req: $msg")
    case msg: Any =>
      log.warning(s"Unknown message: $msg from <$sender>" )
  }

  private def processMarketChange(depth: MarketInfo): Unit = {
    val exchange = depth.exchange
    val singleExchangeRates = rates.getOrElse(exchange, Map())
    rates = rates + (exchange -> (singleExchangeRates + (depth.currencyPair -> depth)))
    val spreadsFuture = calculateSpreads(depth)
    spreadsFuture.foreach { spreads =>
      for(spread <- spreads) {
        log.info(s"sending spread information $spreads to deduplicator")
        sendToDeduplicator(spread)
      }
    }
  }

  /**
   * Sends the spread data to deduplicate
   * @param spread Spread data
   */
  private def sendToDeduplicator(spread: MarketSpread) {
    val key = (spread.market1.currencyPair, spread.exchanges)
    val deduplicator = deduplicators.getOrElse(key, {
      createDeduplicator(key)
    })
    deduplicators += key -> deduplicator
    deduplicator ! spread
  }

  def createDeduplicator(key: (CurrencyPair, (Exchange, Exchange))): ActorRef = {
    log.info(s"Creating new deduplicating actor for $key")
    system.actorOf(SpreadDeduplicationActor.props(key._1, key._2._1, key._2._2),
      s"${key._1.primary}_${key._1.secondary}-${key._2._1}_${key._2._2}")
  }

  /**
   * Calculate spreads between updated pair and other exchanges.
   */
  private def calculateSpreads(reference: MarketInfo): Future[List[MarketSpread]] = {
    //other exchanges
    val targetExchanges = rates.keys.toList.filter(_ != reference.exchange)

    //find matching pairs
    val matching = targetExchanges.map { target =>
      rates(target).filter { kv =>
        val (pair, _) = kv
        pair == reference.currencyPair
      }
    }

    val candidates = matching.flatMap(_.values)

    val spreads = Future.sequence(candidates.map { candidate =>
      validateCandidate(reference, candidate)})
    spreads
  }

  private def validateCandidate(reference: MarketInfo, candidate: MarketInfo): Future[MarketSpread] = {
    for {
      referenceAccountFee <- calculateCommission(reference.currencyPair, reference.exchange)
      candidateAccountFee <- calculateCommission(candidate.currencyPair, candidate.exchange)
    } yield {
      log.debug(s"commissions collected for $reference and $candidate")
      MarketSpread.from(reference -> referenceAccountFee, candidate -> candidateAccountFee)
    }
  }


  private def calculateCommission(pair: CurrencyPair, exchange: Exchange): Future[BigDecimal] = {
    val actor = exchangeApiActors(exchange)
    implicit val timeout = new Timeout(5 seconds)
    val resultFuture = (actor ? TransactionFeeRequest(pair, exchange)).flatMap {
      case response: TransactionFeeResponse => Future.successful(response.fee)
      case err: ErrorResponse =>
        Future.failed(new Exception(err.message))
    }
    resultFuture.onFailure{
      case e: AskTimeoutException =>
        log.error(s"Timeout waiting for the current fee value for $pair from $exchange")
      case _ => log.error("Other exception when asking for fee")
    }
    resultFuture
  }
}
