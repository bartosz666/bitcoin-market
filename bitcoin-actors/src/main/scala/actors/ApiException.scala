package actors

/**
 * Exception thrown due to API error
 */
class ApiException(msg: String, cause: Throwable) extends Exception(msg, cause) {

  def this(msg: String) = this(msg, null)

}
