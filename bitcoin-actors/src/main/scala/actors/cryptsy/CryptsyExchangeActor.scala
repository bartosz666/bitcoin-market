package actors.cryptsy

import akka.actor.{ActorRef, Props, ActorLogging, Actor}
import actors.ExchangeBaseActor
import com.typesafe.config.Config

object CryptsyExchangeActor {

  def props(config: Config) = Props(classOf[CryptsyExchangeActor], config)

  val exchange = "Cryptsy"
}

class CryptsyExchangeActor(config: Config) extends ExchangeBaseActor {

  import context._

  val marketResolver = actorOf(CryptsyMarketResolverActor.props, "marketResolver")

  override val depthActor = actorOf(CryptsyDepthActor.props(marketResolver), "reference")

  override val feeActor = actorOf(CryptsyCalculateFeeActor.props, "fee")

  override val currencyPairsActor: ActorRef = marketResolver

  override val openOrdersActor = actorOf(OrdersListActor.props(config, marketResolver), "open-orders")
}
