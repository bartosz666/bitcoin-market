package actors.mint

import akka.actor.{ActorLogging, Actor, Props}
import akka.pattern.pipe
import scala.collection.immutable.Seq
import scala.concurrent.Future
import spray.http._
import spray.client.pipelining._
import spray.httpx.unmarshalling._
import spray.json._
import spray.http.HttpRequest
import models.CurrencyPair
import scala.concurrent.ExecutionContext.Implicits.global
import actors.ExchangeAPI.{CurrencyPairsResponse, CurrencyPairsRequest}

object TradingPairsActor {

  def props = Props(classOf[TradingPairsActor])

}


/**
 * Collects all trading paris data.
 */
class TradingPairsActor
  extends Actor
  with ActorLogging {

  val apiURL = "https://api.mintpal.com/v1/market/summary/"

  implicit val unmarshaller = new CurrenciesUnmarshaller

  val pipeline: HttpRequest => Future[Seq[CurrencyPair]] =
    sendReceive ~> unmarshal[Seq[CurrencyPair]]

  override def receive: Receive = {
    case CurrencyPairsRequest =>
      val responseFuture = pipeline(Get(apiURL))
      responseFuture.map(CurrencyPairsResponse) pipeTo sender

  }

  class CurrenciesUnmarshaller extends Unmarshaller[Seq[CurrencyPair]] with JsonReader[Seq[CurrencyPair]] {
    import actors.JsonExtensions._

    override def read(json: JsValue): Seq[CurrencyPair] = {
      val currencyValues = json.asInstanceOf[JsArray]
      val pairs = currencyValues.elements.map {
        case jsObject: JsObject =>
          val primary = jsObject.fields("code").stringValue
          val secondary = jsObject.fields("exchange").stringValue
          CurrencyPair(primary, secondary)
      }
      pairs
    }

    override def apply(entity: HttpEntity): Deserialized[Seq[CurrencyPair]] = {
      val parser = JsonParser(entity.asString)
      try {
        Right(parser.convertTo[Seq[CurrencyPair]](this))
      } catch {
        case ex: Throwable => Left(MalformedContent("Could not unmarshal API response", ex))
      }
    }
  }

}
