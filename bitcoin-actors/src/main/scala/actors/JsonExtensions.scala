package actors

import spray.json._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat


object JsonExtensions {

  implicit class RichJsValue(val jsValue: JsValue) extends AnyVal {

    /**
     * Converts arbitrary
     */
    def toBigDecimal: BigDecimal = {
      jsValue match {
        case num: JsNumber => num.value
        case str: JsString => BigDecimal(str.value)
        case msg => throw new IllegalArgumentException(s"Incompatible JSON type $msg")
      }
    }

    def toInt: Int = {
      jsValue match {
        case num: JsNumber => num.value.intValue()
        case str: JsString => str.value.toInt
        case msg => throw new IllegalArgumentException(s"Incompatible JSON type $msg")
      }
    }

    def toBoolean: Boolean = {
      jsValue match {
        case bool: JsBoolean => bool.value
        case str: JsString => str.value.toBoolean
        case num: JsNumber => num.value.intValue() != 0
        case msg => throw new IllegalArgumentException(s"Incompatible JSON type $msg")
      }
    }


    def stringValue: String = {
      jsValue match {
        case str: JsString => str.value
        case other => other.toString()
      }
    }

    def parseUsing[T](f: JsValue => T): T = f(jsValue)

    def mapString[That](f: PartialFunction[String, That]): That = jsValue match {
      case jsString: JsString => f(jsString.value)
      case other => f(other.toString())
    }

  }

}


object JsonConversions {
  import JsonExtensions._

  implicit def toJsArray(jsValue: JsValue) = jsValue match {
    case jsArray: JsArray => jsArray
    case _ => throw new DeserializationException("Not an array")
  }

  implicit def giveJsObject(jsValue: JsValue): JsObject = jsValue.asJsObject("Not an object")

  implicit def giveInt(jsValue: JsValue): Int = {
    jsValue match {
      case num: JsNumber => num.value.intValue()
      case str: JsString => str.value.toInt
      case msg => throw new IllegalArgumentException(s"Incompatible JSON type $msg")
    }
  }

  implicit def giveBoolean(jsValue: JsValue): Boolean = jsValue.toBoolean

  implicit def giveBigDecimal(jsValue: JsValue) = jsValue.toBigDecimal

  implicit def giveString(jsValue: JsValue): String = jsValue match {
    case jsString: JsString => jsString.value
    case _ => jsValue.toString()
  }

  implicit def giveDateTime(jsValue: JsValue) = jsValue match {
    case jsString: JsString =>
      DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(jsString.value)
    case _ =>
      throw new DeserializationException("Expected String value")
  }

}