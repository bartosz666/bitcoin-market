package actors.btce

import akka.actor.{Props, ActorLogging, Actor}
import spray.http._
import spray.httpx.unmarshalling._
import spray.json._
import spray.http.HttpRequest
import actors.ExchangeAPI.{ExchangeOrder, ErrorResponse, MarketInfo, MarketInfoRequest}
import models.CurrencyPair
import akka.pattern.pipe
import spray.client.pipelining._
import scala.concurrent.Future

object BtceDepthActor {

  def props = Props(classOf[BtceDepthActor])

}

/**
 * Collects reference of exchange from the closest asks and bids.
 */
class BtceDepthActor extends Actor with ActorLogging {

  val apiUrl = (pair: CurrencyPair) =>
    s"https://btc-e.com/api/2/${pair.primary.toLowerCase}_${pair.secondary.toLowerCase}/depth"

  implicit val system = context.system

  import system.dispatcher

  val pipeline: HttpRequest => Future[HttpResponse] = sendReceive

  override def receive: Actor.Receive = {
    case req @ MarketInfoRequest(pair, _) =>
      log.info(s"Collecting offers for $pair")
      val responseFuture = pipeline(Get(apiUrl(pair)))
      implicit val unmarshaller = new DepthUnmarshaller(pair)
      responseFuture.map { response =>
        val depth = response.entity.as[MarketInfo]
        depth match {
          case Left(ex) =>
            log.error(s"Error parsing message - $ex")
            log.error(response.entity.asString)
            ErrorResponse(req, ex.toString)
          case Right(depth) => {
            log.debug(s"Sending response to recipient $depth")
            depth
          }
        }
      } pipeTo sender
    }


  class DepthUnmarshaller(pair: CurrencyPair) extends Unmarshaller[MarketInfo] with JsonReader[MarketInfo] {

    override def read(json: JsValue): MarketInfo = {
      import actors.JsonExtensions._

      val ask = json.asJsObject.fields("asks") match {
        case asks: JsArray if !asks.elements.isEmpty =>
          val lowestAsk = asks.elements.sortBy(_.asInstanceOf[JsArray]
            .elements.head.toBigDecimal)
            .head.asInstanceOf[JsArray].elements
          val List(askPrice, askVolume) = lowestAsk
          Some(ExchangeOrder(askPrice.toBigDecimal, askVolume.toBigDecimal))
        case _ => None
      }
      val bid = json.asJsObject.fields("bids") match {
        case bids: JsArray if !bids.elements.isEmpty =>
          val highestBid = bids.elements.sortBy(_.asInstanceOf[JsArray]
            .elements.head.toBigDecimal)
            .last.asInstanceOf[JsArray].elements
          val List(bidPrice, bidVolume) = highestBid
          Some(ExchangeOrder(bidPrice.toBigDecimal, bidVolume.toBigDecimal))
        case _ => None
      }
      MarketInfo(pair, "BTCE", ask, bid)
    }

    override def apply(entity: HttpEntity): Deserialized[MarketInfo] = {
      val parser = JsonParser(entity.asString)
      try {
        Right(parser.convertTo[MarketInfo](this))
      } catch {
        case ex: Throwable => Left(MalformedContent("Could not unmarshal API response", ex))
      }
    }
  }

}
