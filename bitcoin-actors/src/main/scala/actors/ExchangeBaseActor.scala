package actors

import akka.actor.{ActorRef, ActorLogging, Actor}
import akka.pattern.{pipe, ask}
import actors.ExchangeAPI._
import scala.concurrent.duration._
import akka.util.Timeout
import actors.ExchangeAPI.StartRepeating
import actors.ExchangeAPI.TransactionFeeRequest
import actors.ExchangeAPI.MarketInfoRequest

trait ExchangeBaseActor extends Actor with ActorLogging {

  import context._

  /**
   * Actor answering to MarketDepthRequest
   */
  val depthActor: ActorRef

  /**
   * Actor answering to TransactionFeeRequest
   */
  val feeActor: ActorRef

  /**
   * Actor answering to CurrencyPairsRequest
   */
  val currencyPairsActor: ActorRef

  val openOrdersActor: ActorRef

  override def receive: Actor.Receive = {

    case msg: MarketInfoRequest =>
      log.debug("Market reference request")
      depthActor forward msg

    case msg: TransactionFeeRequest =>
      log.debug("Transaction fee request")
      feeActor forward msg

    case msg @ CurrencyPairsRequest =>
      log.debug("Currency pairs request")
      currencyPairsActor forward msg

    case PlannedMessage(request, recipient) =>
      log.debug("Executing planned message")
      implicit val timeout = Timeout(30 seconds)
      request match {
        case msg: MarketInfoRequest =>
          (depthActor ? msg) pipeTo recipient
        case msg: TransactionFeeRequest =>
          (feeActor ? msg) pipeTo recipient
        case msg =>
          recipient ! ErrorResponse(msg, "Unknown message type")
      }

    case StartRepeating(request, interval) =>
      log.info(s"Requested repeated updates for $request in $interval)")
      val recipient = sender
      system.scheduler.schedule(0 seconds, interval, self, PlannedMessage(request, recipient))

    case msg @ OrdersListRequest =>
      openOrdersActor forward msg
  }

}


trait FixedFeeExchange
  extends ExchangeBaseActor {

  val fee: BigDecimal

  private def receiveWithFixedFee: Receive = {
    case msg: TransactionFeeRequest =>
      log.debug("Transaction fee request")
      sender ! TransactionFeeResponse(msg.currencyPair, msg.exchange, fee)
  }

  override def receive: Receive = receiveWithFixedFee orElse super.receive

}