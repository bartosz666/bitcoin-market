arbitrageApp = angular.module('pairs', [])

arbitrageApp.controller('PairsController', ($scope, $http) ->

  startWS = ->
    wsUrl = jsRoutes.controllers.ArbitrageController.websocket().webSocketURL()
    $scope.marketSpreads = {}
    $scope.exchanges = {}

    initSpreads = (exchange1, exchange2) ->
      if $scope.marketSpreads[exchange1] == undefined
        $scope.marketSpreads[exchange1] = {}
      if $scope.marketSpreads[exchange2] == undefined
        $scope.marketSpreads[exchange2] = {}


    $scope.socket = new WebSocket(wsUrl)
    $scope.socket.onmessage = (msg) ->
      $scope.$apply( ->
        console.log "received : #{msg.data}"
        data = JSON.parse(msg.data)
        exchange1 = data.market1.exchange
        exchange2 = data.market2.exchange
        initSpreads(exchange1, exchange2, primary)
        $scope.marketSpreads[exchange1] = data
        $scope.marketSpreads[exchange2]= data
      )

  startWS()

) 
