package controllers

import play.api.mvc.{WebSocket, Action, Controller}
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import akka.util.Timeout
import akka.pattern.ask
import scala.concurrent.duration._
import play.api.libs.iteratee.{Iteratee, Concurrent}
import play.api.libs.json._
import rx.lang.scala.Observer
import play.api.Logger
import actors.OrdersPublisherActor.StartPublishing
import models.{DataChannel, MarketSpread}
import scala.collection.immutable.Seq
import actors.ExchangeAPI.MarketInfo
import akka.actor.ActorRef

/**
 * Controller displaying current prices data.
 */
class ArbitrageController(publisherActor: ActorRef) extends Controller {

  val publishChannel = DataChannel.tupled(Concurrent.broadcast[JsValue])

  implicit val orderWriter: Writes[MarketInfo] = Writes( (info: MarketInfo) =>
    JsObject(Seq(
      "exchange"          -> JsString(info.exchange.toString),
      "primaryCurrency"   -> JsString(info.currencyPair.primary),
      "secondaryCurrency" -> JsString(info.currencyPair.secondary),
      "bidPrice"          -> info.bid.map(bid => JsNumber(bid.price)).orElse(Some(JsNull)).get,
      "bidAmount"         -> info.bid.map(bid => JsNumber(bid.volume)).orElse(Some(JsNull)).get,
      "askPrice"          -> info.ask.map(ask => JsNumber(ask.price)).orElse(Some(JsNull)).get,
      "askAmount"         -> info.ask.map(ask => JsNumber(ask.volume)).orElse(Some(JsNull)).get
    ))
  )

  implicit val operationWriter: Writes[MarketSpread] = Writes( (op: MarketSpread) =>
    Json.obj(
      "market1" -> Json.toJson(op.market1),
      "market2" -> Json.toJson(op.market2),
      "created" -> JsString(op.start.toString("yyyy-MM-dd HH:mm:ss")),
      "spread"  -> JsNumber(op.spread)
    )
  )

  val observer = Observer[MarketSpread]( (op: MarketSpread) =>
    if(op.actionable) {
      publishChannel.channel.push(
        Json.toJson(op)
      )
    }
  )

  implicit val timeout = Timeout(3 seconds)

  val subscriptionResponse = publisherActor ? StartPublishing(0, observer)


  def index = Action.async {
    val result = Future(Ok(views.html.arbitrage()))
    result
  }


  def websocket = WebSocket.using[JsValue] { request =>
    Logger.info("Opening websocket")
    val in = Iteratee.ignore[JsValue]
    val out = publishChannel.enumerator
    (in, out)
  }


}
