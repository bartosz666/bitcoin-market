package controllers

import _root_.storage.UserRecord
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits._
import play.api.Routes
import play.api.data._
import play.api.data.Forms._

class AppController(userRecord: UserRecord) extends Controller with Secured {

  val userForm = Form(
    tuple(
      "username" -> text,
      "password" -> text
    )
  )

  def index = Action {
    implicit request =>
      Ok(views.html.index())
  }

  def login = Action { implicit request =>
    Ok(views.html.login())
  }

  def signin = Action.async { implicit request =>
    val (username, password) = userForm.bindFromRequest.get
    val userFuture = userRecord.userByName(username)
    userFuture map {
      case Some(user) if user.checkPassword(password) =>
        Redirect(routes.MarketSpreadsController.index).withSession(Security.username -> user.username)
      case _ => Redirect(routes.AppController.login)
    }

  }

  def javascriptRoutes = Action {
    implicit request =>
      Ok(
        Routes.javascriptRouter("jsRoutes")(
          routes.javascript.AppController.login,
          routes.javascript.ArbitrageController.index,
          routes.javascript.ArbitrageController.websocket
        )
      ).as("text/javascript")
  }

}
