package controllers

import play.api.mvc._
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.mvc.Results._
import scala.concurrent.Future
import play.api.libs.iteratee.{Enumerator, Iteratee}
import play.api.libs.json.{Json, JsValue}
import scala.Some
import play.api.mvc.WebSocket.HandlerProps
import akka.actor.{ActorRef, Props}
import play.api.Play.current


trait Secured {
  def username(request: RequestHeader): Option[String] = {
    //verify or create session, this should be a real login
    request.session.get(Security.username)
  }

  /**
   * When user not have a session, this function redirects to login page
   */
  def unauthF(request: RequestHeader) = {
    Redirect(routes.AppController.login)
  }

  /**
   * Basi authentication system
   * try to retieve the username, call f() if it is present,
   * or unauthF() otherwise
   */
  def withAuth(f: => String => Request[_ >: AnyContent] => Result): EssentialAction = {
    Security.Authenticated(username, unauthF) {
      username =>
        Action(request => f(username)(request))
    }
  }


  /**
   * Basic authentication system
   * try to retrieve the username, call f() if it is present,
   * or unauthF() otherwise
   */
  def asyncWithAuth(f: => String => Request[_ >: AnyContent] => Future[SimpleResult]): EssentialAction = {
    Security.Authenticated(username, unauthF) {
      username =>
        Action.async(request => f(username)(request))
    }
  }

  /**
   * This function provide a basic authentication for
   * WebSocket, lekely withAuth function try to retrieve the
   * the username form the session, and call f() function if find it,
   * or create an error Future[(Iteratee[JsValue, Unit], Enumerator[JsValue])])
   * if username is none
   */
  def withAuthWS(f: => (String, ActorRef) => Props): WebSocket[JsValue, JsValue] = {
    WebSocket.acceptWithActor[JsValue, JsValue] {
      request => out =>
        username(request) match {
          case None =>
            throw new RuntimeException("User not identified")

          case Some(id) =>
            f(id, out)

        }
    }
  }
}

