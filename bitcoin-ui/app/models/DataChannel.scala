package models

import play.api.libs.iteratee.Enumerator
import play.api.libs.json.JsValue
import play.api.libs.iteratee.Concurrent.Channel

case class DataChannel(enumerator: Enumerator[JsValue], channel: Channel[JsValue])