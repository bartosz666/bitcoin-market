package bootstrap

import com.typesafe.config.Config
import storage.{UserRecord, MarketSpreadRecord, CassandraClient}
import scala.concurrent.Await
import scala.concurrent.duration._
import play.api.Logger

/**
 * Data access module
 */
class DataModule(config: Config) {

  private[bootstrap] lazy val client = new CassandraClient(config)

  lazy val marketSpreadRecord = new MarketSpreadRecord(client)

  lazy val userRecord = new UserRecord(client)

  client.createSchema(marketSpreadRecord, userRecord)



}
