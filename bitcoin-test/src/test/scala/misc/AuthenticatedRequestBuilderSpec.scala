package misc

import org.specs2.mutable.Specification
import common.AuthenticatedRequestBuilder

class AuthenticatedRequestBuilderSpec
  extends Specification {

  "builder" should {

    "produce good signature" in {
      val public = "13DMMIEJ-9BIS37SQ-BTZRB67O-XPFPYB95-E1AHKKP9"
      val secret = "60722e6a69bae05dd35e252c0b4efc2cee16f1791c44a06bb8a1e1c968ea207b"
      val expectedSign =
        "7a184bdca967af758de9244f748a4dce6eac9f5f09bf08dedeeee2fffafb28aae98b03b43b1e0aa23f5c47a7ec34bac240d91c988b93ec78c15d3bd763a4a0fa"
      val builder = new AuthenticatedRequestBuilder("http://foo/bar", public, secret) {
        override def nonce = 1398712856
      }
      val request = builder.buildRequest("method" -> "ActiveOrders")

      val signHeader = request.headers.find(_.name == "SIGN")
      signHeader.isDefined must beTrue
      signHeader.get.value must be_==(expectedSign)

      val keyHeader = request.headers.find(_.name == "KEY")
      keyHeader.isDefined must beTrue
      keyHeader.get.value must be_==(public)

      request.entity.asString must be_==("nonce=1398712856&method=ActiveOrders")

    }

  }

}
