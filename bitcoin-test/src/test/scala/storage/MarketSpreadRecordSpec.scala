package storage

import org.specs2.mutable.{BeforeAfter, Specification}
import models._
import scala.concurrent.Await
import scala.concurrent.duration._
import models.CurrencyPair
import org.joda.time.DateTime
import actors.ExchangeAPI.{ExchangeOrder, MarketInfo}
import com.newzly.phantom.CassandraTable

class MarketSpreadRecordSpec
  extends Specification
  with CassandraInitialization[MarketSpreadRecord] {

  sequential

  "Market Spread record" should {

    "store a record" in {
      val spread = MarketSpread(
        MarketInfo(CurrencyPair("USD", "PLN"), "EX1", Some(ExchangeOrder(1.0, 2.0)), Some(ExchangeOrder(3.0, 4.0))),
        MarketInfo(CurrencyPair("USD", "PLN"), "EX2", Some(ExchangeOrder(5.0, 6.0)), Some(ExchangeOrder(7.0, 8.0))),
        0.1
      )
      val resultFuture = record.persist(spread)
      val result = Await.result(resultFuture, Duration(10, SECONDS))
      result.isFullyFetched must beTrue
    }

    "query the table" in {
      val resultFuture = record.recordsInRange(new DateTime().minusDays(1), new DateTime())
      val result = Await.result(resultFuture, Duration(10, SECONDS))
      result.size must be_==(0)
    }

  }

  override def recordFactory(client: CassandraClient): MarketSpreadRecord =
    new MarketSpreadRecord(client)
}
