package storage

import com.newzly.phantom.CassandraTable
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.slf4j.LazyLogging
import org.specs2.specification.BeforeAfterExample

trait CassandraInitialization[T <: CassandraTable[T, _]]
  extends BeforeAfterExample
  with LazyLogging {

  var client: CassandraClient = _

  val testConfig =
    """
      |storage {
      |  cassandra {
      |    keyspace = "bitcoin_test"
      |    endpoint = "localhost"
      |    port = 9042
      |  }
      |}
    """.stripMargin

  override def before = {
    val config = ConfigFactory.parseString(testConfig)
    client = new CassandraClient(config)
    try {
      client.createSchema(record)
      logger.info("Schema created")
    } catch {
      case ex: Exception => logger.error("oops", ex)
        throw ex
    }

  }

  override def after = {
    client.session.execute(s"DROP KEYSPACE ${client.keySpace}")
    logger.info(s"Schema ${client.keySpace} dropped")
  }

  def recordFactory(client: CassandraClient): T

  lazy val record: T = {
    recordFactory(client)
  }


}
