package storage

import org.specs2.mutable.{BeforeAfter, Before, Specification}
import com.datastax.driver.core.{Cluster, Session}
import scala.concurrent.Await
import scala.concurrent.duration._
import models._
import models.ArbitrageOperation
import models.OrderRequest
import scala.concurrent.ExecutionContext.Implicits.global
import org.specs2.matcher.Matchers
import models.CurrencyPair
import models.ArbitrageOperation
import models.OrderRequest
import org.joda.time.DateTime

class ArbitrageOperationRecordSpec
  extends Specification
  with CassandraInitialization[ArbitrageOperationRecord] {

  "Arbitrage marketSpread record" should {

    "store a record" in {
      implicit val session = client.session

      val operation = ArbitrageOperation(
        OrderRequest(Buy, "test", CurrencyPair("USD", "PLN"), 10.0, 1.0),
        OrderRequest(Sell, "test", CurrencyPair("USD", "PLN"), 10.0, 1.0))
      val resultFuture = ArbitrageOperationRecord.storeRecord(operation)
      val result = Await.result(resultFuture, Duration(10, SECONDS))
      result.isFullyFetched must beTrue
    }

    "query the table" in {
      implicit val session = client.session
      val resultFuture = ArbitrageOperationRecord.recordsInRange(new DateTime().minusDays(1), new DateTime())
      val result = Await.result(resultFuture, Duration(10, SECONDS))
      result.size must be_>(0)
    }

  }

  override def recordFactory(client: CassandraClient): ArbitrageOperationRecord = {
    ArbitrageOperationRecord
  }
}
