package actors

/**
 * Created by fox on 09/03/14.
 */
object TestKitConfig {

  val config =
    """
      |spray.can {
      |  server {
      |    max-encryption-chunk-size = 5m
      |    request-timeout = 200 s
      |    request-chunk-aggregation-limit = 5m
      |  }
      |
      |  client {
      |    max-encryption-chunk-size = 5m
      |    request-timeout = 300 s
      |    response-chunk-aggregation-limit = 5m
      |
      |    parsing {
      |      max-chunk-size             = 5m
      |    }
      |  }
      |}
      |
      |cryptsy {
      |  publicKey=608a216b6251543388c0c8194eabfd6ff0e33409
      |  privateKey=bef0ffd5b9523128d7d5ea0c35bbce33d26135d0cb82e53aa5ab192d67f9b3b221c62a2f4caa7b4e
      |}
    """.stripMargin
}
