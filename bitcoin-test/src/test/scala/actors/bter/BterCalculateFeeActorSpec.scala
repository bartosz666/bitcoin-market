package actors.bter

import org.specs2.mutable.SpecificationLike
import akka.testkit.{TestProbe, TestKit}
import akka.actor.ActorSystem
import models.CurrencyPair
import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory
import models.CurrencyPairConversions._

class BterCalculateFeeActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike {


  "BER fee actor" should {
    "Fetch fee information" in {
      val config = ConfigFactory.parseString(
        """
          |bter.fees.default=0.3
        """.stripMargin)
      val dataActor = system.actorOf(BterCalculateFeeActor.props(config))

      import actors.ExchangeAPI._

      val probe = TestProbe()
      probe.send(dataActor, TransactionFeeRequest("BTC" -> "CNY", BterExchangeActor.exchange))

      val response = probe.expectMsgClass(FiniteDuration(10, SECONDS), classOf[TransactionFeeResponse])
      response.currencyPair must_== CurrencyPair("BTC", "CNY")
      response.fee must_== BigDecimal(0.3)
    }
  }
}