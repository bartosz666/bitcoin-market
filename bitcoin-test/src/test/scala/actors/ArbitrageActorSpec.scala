package actors

import scala.concurrent.duration._
import org.specs2.mutable.SpecificationLike
import akka.testkit.{TestProbe, TestKit}
import akka.actor.ActorSystem
import models.{MarketSpread, CurrencyPair}
import actors.ExchangeAPI._
import actors.btce.BtceExchangeActor
import actors.cryptsy.CryptsyExchangeActor
import scala.collection.immutable.Seq
import org.specs2.matcher.MatchResult
import models.CurrencyPairConversions._

class ExchangeTestProbe(system: ActorSystem, currencyPairs: Seq[CurrencyPair])
  extends TestProbe(system) {

  def testTimeout = 600 seconds

  def expectCurrencyPairsRequest() = {
    expectMsgPF(testTimeout) {
      case CurrencyPairsRequest =>
        sender ! CurrencyPairsResponse(currencyPairs)
    }
  }

  def expectStartUpdating(currencies: Seq[CurrencyPair]) = {
    var count = currencies.size
    fishForMessage(testTimeout) {
      case StartRepeating(request: MarketInfoRequest,_) if currencies.contains(request.currencyPair) =>
        count = count - 1
        count == 0
    }
  }

  def expectFeeMessage(fee: BigDecimal) = {
    expectMsgPF(testTimeout) {
      case TransactionFeeRequest(pair, exchange) =>
        sender ! TransactionFeeResponse(pair, exchange, fee)
    }
  }
}

class ArbitrageActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike {

  def testTimeout = Duration(600, SECONDS)

  def verifySpread(currencies1: Seq[CurrencyPair],
                   market1: MarketInfo,
                   currencies2: Seq[CurrencyPair],
                   market2: MarketInfo,
                   expectedSpread: BigDecimal,
                   fee1: BigDecimal = 0.2,
                   fee2: BigDecimal = 0.2): MatchResult[_] = {
    val market2ExchangeActor = new ExchangeTestProbe(system,currencies2)
    val market1ExchangeActor = new ExchangeTestProbe(system, currencies1)

    val testActor = new TestProbe(system)
    system.eventStream.subscribe(testActor.ref, classOf[MarketSpread])

    val arbitrageActor = system.actorOf(ArbitrageActor.props(Map(
        CryptsyExchangeActor.exchange -> market2ExchangeActor.testActor,
        BtceExchangeActor.exchange -> market1ExchangeActor.testActor
    )))

    market2ExchangeActor.expectCurrencyPairsRequest()
    market1ExchangeActor.expectCurrencyPairsRequest()
    market2ExchangeActor.expectStartUpdating(currencies2.intersect(currencies1))
    market1ExchangeActor.expectStartUpdating(currencies1.intersect(currencies2))
    testActor.expectMsg(testTimeout, Ack)

    market2ExchangeActor.send(arbitrageActor, market2)
    market1ExchangeActor.send(arbitrageActor, market1)
    market1ExchangeActor.expectFeeMessage(fee1)
    market2ExchangeActor.expectFeeMessage(fee2)

    val spread = testActor.expectMsgClass(testTimeout, classOf[MarketSpread])

    spread.market1 must be_==(market1)
    spread.market2 must be_==(market2)
    spread.spread must be_==(expectedSpread)
  }


  "Arbitrage actor" should {

    "initiate exchange actors" in {

      val cryptsyCurrencies = Seq(
        CurrencyPair("BTC", "LTC"),
        CurrencyPair("BTC", "DDD")
      )
      val btceCurrencies = Seq(
        CurrencyPair("BTC", "LTC"),
        CurrencyPair("BTC", "ZZZ")
      )

      val cryptsyExchangeActor = new ExchangeTestProbe(system, cryptsyCurrencies)
      val btceExchangeActor = new ExchangeTestProbe(system, btceCurrencies)

      system.actorOf(ArbitrageActor.props(Map(
        CryptsyExchangeActor.exchange -> cryptsyExchangeActor.testActor,
        BtceExchangeActor.exchange -> btceExchangeActor.testActor
      )))

      cryptsyExchangeActor.expectCurrencyPairsRequest()
      btceExchangeActor.expectCurrencyPairsRequest()
      cryptsyExchangeActor.expectStartUpdating(cryptsyCurrencies.intersect(btceCurrencies))
      btceExchangeActor.expectStartUpdating(btceCurrencies.intersect(cryptsyCurrencies))

      success("All messages delivered as expected")
    }

    //------------------------------------------------------------

    "Sell 0.1 LTCBTC at 0.02569 &Buy 0.1 LTCBTC at 0.02544" in {

      val cryptsyCurrencies = Seq(
        CurrencyPair("BTC", "LTC"),
        CurrencyPair("BTC", "DDD")
      )
      val btceCurrencies = Seq(
        CurrencyPair("BTC", "LTC"),
        CurrencyPair("BTC", "ZZZ")
      )

      val market1 = MarketInfo(
        "BTC" -> "LTC",
        BtceExchangeActor.exchange,
        Some(ExchangeOrder(price = 0.02544, volume = 0.1)),
        Some(ExchangeOrder(price = 0.02542, volume = 30))
      )
      val market2 = MarketInfo(
        "BTC" -> "LTC",
        CryptsyExchangeActor.exchange,
        Some(ExchangeOrder(price = 0.02574, volume = 30)),
        Some(ExchangeOrder(price = 0.02569, volume = 30))
      )

      verifySpread(btceCurrencies, market1, cryptsyCurrencies, market2, BigDecimal(-0.00014774))
    }

    //------------------------------------------------------------

    "not find arbitrage against for given update" in {

      val btceCurrencies = Seq(
        CurrencyPair("BTC", "LTC"),
        CurrencyPair("BTC", "ZZZ")
      )
      val cryptsyCurrencies = Seq(
        CurrencyPair("BTC", "LTC"),
        CurrencyPair("BTC", "DDD")
      )
      val market1 = MarketInfo(
        "BTC" -> "LTC",
        BtceExchangeActor.exchange,
        Some(ExchangeOrder(price = 0.02569, volume = 30)),
        Some(ExchangeOrder(price = 0.02446, volume = 30))
      )
      val market2 = MarketInfo(
        "BTC" -> "LTC",
        CryptsyExchangeActor.exchange,
        Some(ExchangeOrder(price = 0.02445, volume = 30)),
        Some(ExchangeOrder(price = 0.02441, volume = 30))
      )
      verifySpread(btceCurrencies, market1, cryptsyCurrencies, market2, BigDecimal(0))
    }

    //------------------------------------------------------------

    "Buy 30 LTCBTC at 0.02445 & Sell 30 LTCBTC at 0.02566" in {

      val cryptsyCurrencies = Seq(
        CurrencyPair("BTC", "LTC"),
        CurrencyPair("BTC", "DDD")
      )
      val btceCurrencies = Seq(
        CurrencyPair("BTC", "LTC"),
        CurrencyPair("BTC", "ZZZ")
      )
      val market2 = MarketInfo(
        "BTC" -> "LTC",
        CryptsyExchangeActor.exchange,
        Some(ExchangeOrder(price = 0.02445, volume = 30)),
        Some(ExchangeOrder(price = 0.02441, volume = 30))
      )
      val market1 = MarketInfo(
        "BTC" -> "LTC",
        BtceExchangeActor.exchange,
        Some(ExchangeOrder(price = 0.02569, volume = 30)),
        Some(ExchangeOrder(price = 0.02566, volume = 30))
      )
      verifySpread(btceCurrencies, market1,
        cryptsyCurrencies, market2,
        BigDecimal(0.00123566),
        -0.1,
        0.0
      )
    }


  }
}