package actors.mint

import org.specs2.mutable.{SpecificationLike, Specification}
import akka.testkit.{TestProbe, TestKit}
import akka.actor.ActorSystem
import org.specs2.matcher.Matchers
import actors.ExchangeAPI.{MarketInfo, OrderListResponse, MarketInfoRequest}
import models.CurrencyPairConversions._
import org.specs2.time.NoTimeConversions
import scala.concurrent.duration._
import models.CurrencyPair

class MintDepthActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike
  with Matchers
  with NoTimeConversions {

  "Mintpal depth actor" should {

    "collect current market data" in {
      val actor = system.actorOf(MintDepthActor.props)
      val probe = new TestProbe(system)
      probe.send(actor, MarketInfoRequest("LTC" -> "BTC", MintExchangeActor.exchange))
      val msg = probe.expectMsgClass(20 seconds, classOf[MarketInfo])
      msg.currencyPair must be_==(CurrencyPair("LTC", "BTC"))
    }

  }


}
