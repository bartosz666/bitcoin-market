package actors.cryptsy

import org.specs2.mutable.Specification
import org.specs2.matcher.Matchers

class CryptsyAuthenticatedRequestBuilderSpec extends Specification with Matchers {

  "cryptsy private request" should {
    "build a request" in {
      val publicKey="608a216b6251543388c0c8194eabfd6ff0e33409"
      val privateKey="bef0ffd5b9523128d7d5ea0c35bbce33d26135d0cb82e53aa5ab192d67f9b3b221c62a2f4caa7b4e"

      val builder = new CryptsyAuthenticatedRequestBuilder(publicKey = publicKey, privateKey = privateKey)

      val request = builder.buildRequest(
        "info",
        "method" -> "reference",
        "marketid" -> "112")

      request.headers.exists(_.name == "Sign") must beTrue
      request.headers.exists(_.name == "Key") must beTrue
    }
  }

}
