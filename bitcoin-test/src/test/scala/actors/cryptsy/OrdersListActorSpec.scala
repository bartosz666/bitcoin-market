package actors.cryptsy

import org.specs2.mutable.{SpecificationLike, Specification}
import akka.testkit.{TestProbe, TestKit}
import akka.actor.ActorSystem
import com.typesafe.config.{ConfigValueFactory, ConfigValue, ConfigFactory, Config}
import actors.cryptsy.CryptsyMarketResolverActor.{CurrencyMappingResponse, CurrencyMapping}
import org.specs2.time.NoTimeConversions
import scala.concurrent.duration._
import models.CurrencyPair
import actors.ExchangeAPI.{OrderListResponse, OrdersListRequest}

class OrdersListActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike
  with NoTimeConversions {

  def config: Config = ConfigFactory.parseString(
    """
      |cryptsy {
      |    public-key = "608a216b6251543388c0c8194eabfd6ff0e33409"
      |    private-key = "bef0ffd5b9523128d7d5ea0c35bbce33d26135d0cb82e53aa5ab192d67f9b3b221c62a2f4caa7b4e"
      |}
    """.stripMargin)

  "Order list" should {
    "return all orders" in {
      val marketIdActorProbe = new TestProbe(system)
      val probe = new TestProbe(system)
      val actor = system.actorOf(OrdersListActor.props(config, marketIdActorProbe.ref))
      marketIdActorProbe.expectMsg(1 second, CurrencyMapping)
      marketIdActorProbe.reply(CurrencyMappingResponse(List(
        CurrencyPair("BTC", "USD") -> 1,
        CurrencyPair("DOGE", "USD") -> 2
      )))
      probe.send(actor, OrdersListRequest)
      val response = probe.expectMsgClass(20 seconds, classOf[OrderListResponse])
      response.exchange must_== "Cryptsy"
    }
  }

}
