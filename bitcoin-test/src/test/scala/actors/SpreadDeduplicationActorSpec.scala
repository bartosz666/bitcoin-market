package actors

import org.specs2.mutable.SpecificationLike
import akka.testkit.{TestProbe, TestFSMRef, TestKit}
import akka.actor.ActorSystem
import models.CurrencyPairConversions._
import actors.SpreadDeduplicationActor._
import models.MarketSpread
import actors.ExchangeAPI.{ExchangeOrder, MarketInfo}
import actors.SpreadDeduplicationActor.MarketSpreadWindowOpen
import actors.ExchangeAPI.ExchangeOrder
import actors.ExchangeAPI.MarketInfo
import scala.Some

class SpreadDeduplicationActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike {

  def marketSpread(spread: BigDecimal): MarketSpread = {
    MarketSpread(
      MarketInfo("ABC" -> "DEF", "EX1", Some(ExchangeOrder(2,10)), Some(ExchangeOrder(1,10))),
      MarketInfo("ABC" -> "DEF", "EX2", Some(ExchangeOrder(2,10)), Some(ExchangeOrder(1,10))),
      spread)
  }

  "Deduplicator" should {

    "ignore 0 spread when Closed" in {
      val fsm = TestFSMRef(new SpreadDeduplicationActor("ABC" -> "DEF", "EX1", "EX2"))
      fsm.stateName must_== Closed
      fsm.stateData must_== Uninitialized
      fsm ! marketSpread(0)

      fsm.stateName must_== Closed
      fsm.stateData must_== Uninitialized
    }


    "change state to Open when positive spread appears" in {
      val fsm = TestFSMRef(new SpreadDeduplicationActor("ABC" -> "DEF", "EX1", "EX2"))
      val testProbe = new TestProbe(system)
      system.eventStream.subscribe(testProbe.ref, classOf[MarketSpreadWindowOpen])
      fsm ! marketSpread(1)

      fsm.stateName must_== Open
      fsm.stateData must beAnInstanceOf[MarketSpreadWindowOpen]
      val window = testProbe.expectMsgClass(classOf[MarketSpreadWindowOpen])
      window.spread.end.isDefined must beFalse
    }

    "change state to Closed when not positive spread appears" in {
      val fsm = TestFSMRef(new SpreadDeduplicationActor("ABC" -> "DEF", "EX1", "EX2"))
      val testProbe = new TestProbe(system)
      system.eventStream.subscribe(testProbe.ref, classOf[MarketSpreadWindow])
      fsm ! marketSpread(1)
      testProbe.expectMsgClass(classOf[MarketSpreadWindowOpen])
      fsm.stateName must_== Open

      fsm ! marketSpread(0)
      fsm.stateName must_== Closed
      val window = testProbe.expectMsgClass(classOf[MarketSpreadWindowClosed])
      window.spread.end.isDefined must beTrue
    }

    "just open state" in {
      val fsm = TestFSMRef(new SpreadDeduplicationActor("ABC" -> "DEF", "EX1", "EX2"))
      val testProbe = new TestProbe(system)
      system.eventStream.subscribe(testProbe.ref, classOf[MarketSpreadWindow])
      fsm.stateName must_== Closed
      fsm.stateData must_== Uninitialized
      fsm ! MarketSpread(
        MarketInfo("FTC" -> "BTC","BTCE",Some(ExchangeOrder(0.00018,757.32401113)),Some(ExchangeOrder(0.00017001,140.086))),
        MarketInfo("FTC" -> "BTC","Cryptsy",Some(ExchangeOrder(0.00016860,0.00336902)),Some(ExchangeOrder(0.00016808,218.90249639))),
        7.3278E-7)

      fsm.stateName must_== Open
    }


  }


}
