package actors.btce

import org.specs2.mutable.SpecificationLike
import akka.testkit.{TestProbe, TestKit}
import akka.actor.ActorSystem
import models.CurrencyPair
import scala.concurrent.duration._
import models.CurrencyPairConversions._

class BtceCalculateFeeActorSpec   extends TestKit(ActorSystem("testsystem"))
with SpecificationLike {

  sequential

  "BTCE fee actor" should {

    "Fetch fee value from API" in {
      val dataActor = system.actorOf(BtceCalculateFeeActor.props)

      import actors.ExchangeAPI._

      val probe = TestProbe()
      probe.send(dataActor, TransactionFeeRequest("BTC" -> "USD", BtceExchangeActor.exchange))

      val msg = probe.expectMsgClass(FiniteDuration(10, SECONDS), classOf[TransactionFeeResponse])
      msg.fee must be_>=(BigDecimal(0))
    }

    "Fetch list of currencies" in {
      val dataActor = system.actorOf(BtceCalculateFeeActor.props)

      import actors.ExchangeAPI._

      val probe = TestProbe()
      probe.send(dataActor, CurrencyPairsRequest)

      val msg = probe.expectMsgClass(FiniteDuration(20, SECONDS), classOf[CurrencyPairsResponse])
      msg.pairs.isEmpty must beFalse
    }


  }
}