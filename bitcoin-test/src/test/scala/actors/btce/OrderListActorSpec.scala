package actors.btce

import org.specs2.mutable.SpecificationLike
import akka.testkit.{TestProbe, TestKit}
import akka.actor.{Props, ActorSystem}
import org.specs2.time.NoTimeConversions
import com.typesafe.config.ConfigFactory
import scala.concurrent.duration._
import actors.ExchangeAPI.{OrderListResponse, OrdersListRequest}

class OrderListActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike
  with NoTimeConversions {

  "Order list actor" should {

    "execute real API call" in {
      val config = ConfigFactory.parseString(
        """
          |btce {
          |  public-key = "13DMMIEJ-9BIS37SQ-BTZRB67O-XPFPYB95-E1AHKKP9"
          |  private-key = "60722e6a69bae05dd35e252c0b4efc2cee16f1791c44a06bb8a1e1c968ea207b"
          |}
        """.stripMargin)
      val actor = system.actorOf(Props(new OrderListActor(config)))
      val probe = TestProbe()
      probe.send(actor, OrdersListRequest)
      val response = probe.expectMsgClass(2000 seconds, classOf[OrderListResponse])
      response.exchange must be_==("BTCE")
      response.orders.size must be_==(0)
    }


  }
}