package actors.btce

import org.specs2.mutable.{After, SpecificationLike}
import akka.testkit.{TestProbe, TestKit}
import akka.actor.ActorSystem
import models.CurrencyPair
import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory
import models.CurrencyPairConversions._

class BtceExchangeActorSpec
  extends TestKit(ActorSystem("testsystem"))
  with SpecificationLike {

  sequential

  def config = ConfigFactory.parseString(
      """
        |btce {
        |    public-key = "13DMMIEJ-9BIS37SQ-BTZRB67O-XPFPYB95-E1AHKKP9"
        |    private-key = "60722e6a69bae05dd35e252c0b4efc2cee16f1791c44a06bb8a1e1c968ea207b"
        |}
      """.stripMargin)


  "BTCE Exchange actor" should {

    "Fetch reference data from API" in {
      val exchangeActor = system.actorOf(BtceExchangeActor.props(config))

      import actors.ExchangeAPI._

      val probe = TestProbe()
      probe.send(exchangeActor, MarketInfoRequest("BTC" -> "USD", BtceExchangeActor.exchange))

      val response = probe.expectMsgClass(FiniteDuration(10, SECONDS), classOf[MarketInfo])
      response.currencyPair must_== CurrencyPair("BTC", "USD")
    }

    "Fetch fee value from API" in {
      val exchangeActor = system.actorOf(BtceExchangeActor.props(config))

      import actors.ExchangeAPI._

      val probe = TestProbe()
      probe.send(exchangeActor, TransactionFeeRequest("BTC" -> "USD", BtceExchangeActor.exchange))

      val response = probe.expectMsgClass(FiniteDuration(10, SECONDS), classOf[TransactionFeeResponse])
      response.currencyPair must_== CurrencyPair("BTC", "USD")
      response.fee must_== BigDecimal(0.2)
    }
  }

}
