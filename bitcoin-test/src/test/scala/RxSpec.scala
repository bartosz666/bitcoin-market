import org.specs2.matcher.Matchers
import org.specs2.mutable.Specification
import rx.lang.scala.{Observable, Observer, Subject, Subscription}

class RxSpec extends Specification with Matchers {

  "Subject" should {

    "publish events to subscribers" in {
      val subject: Subject[String] = Subject()
      val observable = subject.publish
      observable.connect
      subject.onNext("one")
      var received: List[String] = Nil
      val subscription = observable.subscribe(Observer(onNext = (next: String) =>
          received = received :+ next
      ))
      subject.onNext("two")
      received must contain("two")
      received must not(contain("one"))
      subscription.unsubscribe()
      subject.onNext("three")
      received must not(contain("three"))

      var received2: List[String] = Nil
      observable.subscribe(Observer(onNext = (next: String) =>
        received2 = received2 :+ next
      ))
      subject.onNext("four")
      received2 must contain("four")
    }



    "cache past events" in {
      val subject: Subject[Int]  = Subject()
      val observable = subject.publish
      observable.connect
      val observableCached = observable.cache
      observableCached.subscribe()
      subject.onNext(1)
      var received: List[Int] = Nil
      observableCached.subscribe(Observer(onNext = (next: Int) =>
          received = received :+ next
      ))
      subject.onNext(2)
      received must contain(1)
      received must contain(2)
    }

  }

}
