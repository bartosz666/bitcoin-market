import sbt._
import Keys._
import play.Play.autoImport._
import PlayKeys._
import com.typesafe.sbt.less.Import.LessKeys
import com.typesafe.sbt.web.SbtWeb
import com.typesafe.sbt.web.Import._
import com.typesafe.sbt.less.Import._

object BitcoinArbitrage extends Build {

  val appName     = "bitcoin-arbitrage"

  val appVersion  = "1.0"

  val scalazVersion = "7.0.6"
  val ioSprayVersion = "1.3.1"
  val phantomVersion = "0.7.0"
  val akkaVersion = "2.3.3"
  val sprayJsonVersion = "1.2.6"

  scalacOptions ++= Seq("-feature")

  val sharedSettings: Seq[Def.Setting[_]] = Seq(
    organization := "com.torfox",
    version := appVersion,
    scalaVersion := "2.10.4",
    resolvers ++= Seq(
      "Typesafe repository snapshots"    at "http://repo.typesafe.com/typesafe/snapshots/",
      "Typesafe repository releases"     at "http://repo.typesafe.com/typesafe/releases/",
      "Sonatype releases"                at "https://oss.sonatype.org/content/repositories/releases",
      "Sonatype snapshots"               at "https://oss.sonatype.org/content/repositories/snapshots",
      "Sonatype repo"                    at "https://oss.sonatype.org/content/groups/scala-tools/",
      "Sonatype staging"                 at "http://oss.sonatype.org/content/repositories/staging",
      "spray repo"                       at "http://repo.spray.io",
      "Java.net Maven2 Repository"       at "http://download.java.net/maven/2/",
      "Twitter Repository"               at "http://maven.twttr.com"
    ),
    scalacOptions ++= Seq(
      "-language:postfixOps",
      "-language:implicitConversions",
      "-language:reflectiveCalls",
      "-language:higherKinds",
      "-language:existentials",
      "-Yinline-warnings",
      "-Xlint",
      "-deprecation",
      "-feature",
      "-unchecked"
    )
  )

  lazy val bitcoin = Project(
    id = "bitcoin",
    base = file("."),
    settings = Defaults.coreDefaultSettings ++ sharedSettings
  ).settings(
    name := "bitcoin"
  ).aggregate(
    bitcoinActors,
    bitcoinTest,
    bitcoinUI
  )

  lazy val bitcoinActors = Project(
    id = "bitcoin-actors",
    base = file("bitcoin-actors"),
    settings = Project.defaultSettings ++
      sharedSettings
  ).settings(
    name := "bitcoin-actors",
    libraryDependencies ++= Seq(
      "com.typesafe.akka"           %% "akka-actor"             % akkaVersion,
      "com.typesafe.akka"           %% "akka-slf4j"             % akkaVersion,
      "io.spray"                    %  "spray-can"              % ioSprayVersion,
      "io.spray"                    %  "spray-client"           % ioSprayVersion,
      "io.spray"                    %  "spray-routing"          % ioSprayVersion exclude("com.chuusai", "shapeless_2.10"),
      "io.spray"                    %% "spray-json"             % sprayJsonVersion,
      "com.chuusai"                 %  "shapeless_2.10.4"       % "2.0.0",
      "com.github.nscala-time"      %% "nscala-time"            % "1.0.0",
      "commons-codec"               %  "commons-codec"          % "1.9",
      "org.scala-lang.modules"      %% "scala-async"            % "0.9.1",
      "com.typesafe.scala-logging"  %% "scala-logging-slf4j"    % "2.1.2",
      "com.netflix.rxjava"          %  "rxjava-scala"           % "0.17.1",
      "com.newzly"                  %% "phantom-dsl"            % phantomVersion,
      "com.typesafe.play"           %% "play-iteratees"         % "2.3.0"
    )
  )


  lazy val bitcoinUI = Project(
    id = "bitcoin-ui",
    base = file("bitcoin-ui"),
    settings = Defaults.coreDefaultSettings ++ sharedSettings
  ).enablePlugins(play.PlayScala, SbtWeb).settings(
    name := "bitcoin-ui",
    libraryDependencies ++= Seq(
      "com.softwaremill.macwire"        %% "macros"         % "0.5",
      "org.webjars"                     %  "bootstrap"      % "3.1.1",
      "org.webjars"                     %  "jquery"         % "2.1.0-2",
      "org.webjars"                     %  "requirejs"      % "2.1.11-1"
    ),
    includeFilter in (Assets, LessKeys.less) := "*.less"
  ).dependsOn(
    bitcoinActors
  )



  lazy val bitcoinData = Project(
    id = "bitcoin-data",
    base = file("bitcoin-data"),
    settings = Defaults.coreDefaultSettings ++ sharedSettings
  ).settings(
      name := "bitcoin-data",
      libraryDependencies ++= Seq(
        "com.github.nscala-time"      %% "nscala-time"            % "1.0.0",
        "commons-codec"               %  "commons-codec"          % "1.9",
        "org.scala-lang.modules"      %% "scala-async"            % "0.9.1",
        "com.typesafe.scala-logging"  %% "scala-logging-slf4j"    % "2.1.2",
        "com.netflix.rxjava"          %  "rxjava-scala"           % "0.17.1",
        "com.newzly"                  %% "phantom-dsl"            % phantomVersion,
        "com.tuplejump"               %% "calliope"               % "0.9.0-U1-C2-EA"
      )
    )


  lazy val bitcoinTest = Project(
    id = "bitcoin-test",
    base = file("bitcoin-test"),
    settings = Defaults.coreDefaultSettings ++
      sharedSettings
  ).settings(
    name := "bitcoin-test",
    fork := true,
    concurrentRestrictions in Test := Seq(
      Tags.limit(Tags.ForkedTestGroup, 4)
    ),
    libraryDependencies ++= Seq(
      "com.typesafe.play"           %% "play-test"                  % "2.3.0",
      "org.scalaz"                  %% "scalaz-scalacheck-binding"  % scalazVersion,
      "com.typesafe.akka"           %% "akka-testkit"               % akkaVersion,
      "io.spray"                    %  "spray-testkit"              % ioSprayVersion,
      "org.specs2"                  %% "specs2"                     % "2.3.12"
    )
  ).dependsOn(
    bitcoinUI
  )


}
